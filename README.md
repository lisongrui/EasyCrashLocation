##使用示例

#### 现在在XX移动统计上面发现错误:

` *** Terminating app due to uncaught exception 'NSRangeException', reason: '*** -[__NSArrayM objectAtIndex:]: index 12 beyond bounds for empty array'`


#### 找到堆栈主要信息

`0x000000010010d4e8 1School + 431336`


#### 在Xcode的Organizer里找到本次的打包文件

`1School 15-8-22 上午9.08.xcarchive`


#### 找到app文件，拷贝出来
![](https://git.oschina.net/lisongrui/EasyCrashLocation/raw/master/image-folder/17BEB9F5-8264-4C3D-A5AF-572E59005F8A.png?dir=0&filepath=image-folder%2F17BEB9F5-8264-4C3D-A5AF-572E59005F8A.png&oid=755c21dc5692d0705eb226b1c9aedc74a5eadf85&sha=6e1dee0008746a21c260fd0885b661dfe55ae6e1)

#### 打开这个App，选择app文件，然后把地址信息填好
![](https://git.oschina.net/lisongrui/EasyCrashLocation/raw/master/image-folder/038AFB05-E1BB-4A6F-8747-3ABFD957E18C.png)

#### 点击"转换地址"，就得到了崩溃的具体位置
![](https://git.oschina.net/lisongrui/EasyCrashLocation/raw/master/image-folder/CD3ED5E2-5E5D-47FB-BDC8-BFAB5CCF651E.png?dir=0&filepath=image-folder%2FCD3ED5E2-5E5D-47FB-BDC8-BFAB5CCF651E.png&oid=e0b5a20ff57a7081c787e6fed15f75de032eb1dd&sha=6e1dee0008746a21c260fd0885b661dfe55ae6e1)